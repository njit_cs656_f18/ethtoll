pragma solidity ^0.4.22;

/// @title Voting with delegation.
contract Toll {
    mapping(address => Voter) public voters;

    struct Voter {
        bool voted;  // if true, that person already voted
        uint vote;   // index of the voted proposal
    }

    struct Proposal {
        bytes32 name;   // short name (up to 32 bytes)
        uint voteCount; // number of accumulated votes
        address maintainer;
        uint tollAmount;
    }

    address public chairperson;

    bool proposalAccepted;
    bool startVote;
    uint totalVoters;
    uint totalVotes;
    uint acceptedProposal;

    Proposal[] public proposals;

    bool invoiceSubmitted;
    uint public currentInvoiceAmount;
    bytes32 public currentInvoiceRecord;


    constructor(bytes32[] proposalNames,
                address[] maintainers,
                uint[] tollAmounts) public {
        require(proposalNames.length == maintainers.length &&
                maintainers.length == tollAmounts.length,
                "Proposals, maintainers and toll amounts must align.");
        chairperson = msg.sender;
        voters[chairperson].voted = false;
        proposalAccepted = false;

        for (uint i = 0; i < proposalNames.length; i++) {
            proposals.push(Proposal({
                name: proposalNames[i],
                maintainer: maintainers[i],
                tollAmount: tollAmounts[i],
                voteCount: 0
            }));
        }
        proposalAccepted = false;
        invoiceSubmitted = false;
        startVote = false;
        totalVoters = 1; // including the chairperson
    }


    function giveRightToVote(address voter) public {
        require(
            msg.sender == chairperson,
            "Only chairperson can give right to vote."
        );
        require(
            !voters[voter].voted,
            "The voter already voted."
        );
        require(
            !startVote,
            "No one can be given right to vote after voting started."
        );
        voters[voter].voted = false;
        totalVoters += 1;
    }

    function startVoting() public {
        require(
            msg.sender == chairperson,
            "Only chairperson can start the vote."
        );
        startVote = true;
    }

    function vote(uint proposal) public {
        require(startVote, "Voting has not yet started.");
        Voter storage sender = voters[msg.sender];
        require(!sender.voted, "Already voted.");
        sender.voted = true;
        sender.vote = proposal;
        proposals[proposal].voteCount += 1;
        totalVotes += 1;
    }

    function winningProposal() public view
            returns (uint winningProposal_)
    {
        uint winningVoteCount = 0;
        for (uint p = 0; p < proposals.length; p++) {
            if (proposals[p].voteCount > winningVoteCount) {
                winningVoteCount = proposals[p].voteCount;
                winningProposal_ = p;
            }
        }
    }

    function endVote() public
    {
        // this could also be ended at a quorum, time-based, or a combination
        require(totalVotes == totalVoters, "Not all voters have voted.");
        acceptedProposal = winningProposal();
        proposalAccepted = true;
    }

    function submitInvoice(uint amount, bytes32 invoiceRecord) public
    {
        require(proposalAccepted, "A proposal has not yet been awarded.");
        require(!invoiceSubmitted, "One invoice submission at a time.");
        require(msg.sender == proposals[acceptedProposal].maintainer,
                "Only the awarded maintainer can submit invoices.");
        require(address(this).balance > amount, "Can't bleed blood from a rock! Wait for more!");
        invoiceSubmitted = true;
        currentInvoiceAmount = amount;
        currentInvoiceRecord = invoiceRecord;
    }

    function approveInvoice() public
    {
        require(proposalAccepted, "A proposal has not yet been awarded.");
        require(msg.sender == chairperson, "Only the chairperson can approve invoices"); 
        require(address(this).balance > currentInvoiceAmount, "Can't bleed blood from a rock! Wait for more!");
        invoiceSubmitted = false;
        proposals[acceptedProposal].maintainer.transfer(currentInvoiceAmount); 
    }

    function payToll() public payable
    {
        require(proposalAccepted, "A proposal has not yet been awarded.");
        require(msg.value == proposals[acceptedProposal].tollAmount, "Not equal to toll amount");
    }

}

# Extending this Work

If you wanted to test your ability to work with Ethereum, you could try to extend this repository to use Truffle and Ganache to create a web interface.  Our group examined Ethereum from a functional perspective only writing tests that wrapped the demonstrated contracts, but if you wanted to create something that an end user could interact with then the drizzle tool in the truffle framework may provide a starting point: https://truffleframework.com/drizzle 

Have fun!

# Installation

You'll need NodeJS installed before any remaining steps.  NodeJS 8+ is recommended (see discussion below).

Installation of truffle can be found here: https://truffleframework.com/docs/truffle/getting-started/installation

One caveat is that Node JS 8+ is recommended, as this repository uses the async/await syntax for tests versus the callback style shown in the Truffle documentation  `someFunction().then(callback)`.  This was chosen as it looks a lot cleaner generally, but they are roughly equivalent.

Installation of the ganache command line interface tool ganache-cli can be located here: https://truffleframework.com/docs/ganache/quickstart

However, Ganache has a UI version here: https://truffleframework.com/ganache which may be fun to play around with if you wanted to do ad-hoc explorations of the transactions, blocks, and logs.  Functionally, it is equivalent to the ganache-cli tool.

# Running these tests

Ganache (either CLI or UI version) should be running before running truffle.

Running the result of a `truffle test` on this repository after proper installation and ganache-cli (ethereum test rpc client) will result in the following after a "clean" initial run of the test network.


```
Using network 'development'.

Compiling ./contracts/Ballot.sol...
Compiling ./contracts/Purchase.sol...


  Contract: Ballot
    Checking constructor parameters
     ✓ should have chairman (66ms)
     ✓ should have three proposals (142ms)
    Voting
     ✓ should be able to be granted to a voter by the chairperson (316ms)
     ✓ a voter can not vote if not permitted (83ms)
     ✓ permit a valid voter the right to delegate to another (412ms)
     ✓ accurately computes the winning proposal (172ms)

Contract: Purchase
before any transaction the seller has 100 ether
before any transaction the buyer has 100 ether
after posting the sale the seller has 97.9329626 ether
after posting the sale the buyer has 100 ether
after posting the sale the contract has 2 ether
    A buyer buys an item for 1 ether with a 1 ether deposit.
and after confirming the transaction the buyer has 97.9951494 ether
using  0.0048506  gas in ether conducting the transaction.
after purchase confirmation the buyer has 97.9951494 ether
after purchase confirmation the seller has 97.9329626 ether
after purchase the sale the contract has 4 ether
after item receipt confirmation the buyer has 98.9907154 ether
after item receipt confirmation the seller has 100.9329626 ether
after item receipt confirmation the contract has 0 ether
     ✓ confirming the purchase whose item value is 1 ether, with a 1 ether deposit (2511ms)


 7 passing (6s)
```

The following is the output explicitly for the Toll road example (`truffle test test/toll.js`).

```
Using network 'development'.

Compiling ./contracts/Toll.sol...

  Contract: Toll
Toll Contract Lifecycle
    ✓ voting granted by chairperson, but chairperson can be outvoted (1672ms)
    ✓ after contract award only permit the awardee the ability to submit invoices (71ms)
    ✓ a car must pay an amount equal to the toll (96ms)
before paying the toll 4 times the a car has 100 ether
before paying the toll 4 times the contract has 0 ether
before paying the toll 4 times the maintainer has 100 ether
after paying the toll 4 times the a car has 95.9909348 ether
after paying the toll 4 times the contract has 4 ether
after paying the toll 4 times the maintainer has 100 ether
    ✓ allows cars to pay tolls after contract award (1800ms)
    ✓ does not allow the maintainer submit invoices for more than the contract (55ms)
before submitting invoice the a car has 95.9909348 ether
before submitting invoice the contract has 4 ether
before submitting invoice the maintainer has 99.9975284 ether
after submitting invoice the a car has 95.9909348 ether
after submitting invoice the contract has 4 ether
after submitting invoice the maintainer has 99.9890391 ether
    ✓ allows the maintainer contract awardee to submit invoices (1612ms)
    ✓ disallows anyone but the chairperson from approving invoices (42ms)
before approving invoice the a car has 95.9909348 ether
before approving invoice the chairperson has 99.7863524 ether
before approving invoice the contract has 4 ether
before approving invoice the maintainer has 99.9868226 ether
after approving invoice the a car has 95.9909348 ether
after approving invoice the chairperson has 99.7842146 ether
after approving invoice the contract has 3 ether
after approving invoice the maintainer has 100.9868226 ether
    ✓ allows the chairperson to approve an invoice (2010ms)


 8 passing (8s)
```


The following is the associated ethereum test network (a client named ganache) running in tandem with the tests driven by the truffle client.

```
Ganache CLI v6.1.8 (ganache-core: 2.2.1)

Available Accounts
==================
(0) 0xe8bd9120d11297a1b2ade7fb08723c4f243d8ccb (~100 ETH)
(1) 0x9766955a706f9460b8c0dee48c683f3552ad7ace (~100 ETH)
(2) 0xb20ba4386cbab2828fca18de198baca27cec6273 (~100 ETH)
(3) 0xdda997e0e0c04e697f43b4753f0990929c1a3cf2 (~100 ETH)
(4) 0xee084c4c1a84beace69658c68fa505bb7891df0c (~100 ETH)
(5) 0x024b2f47e17088d1aa49060e83c02fbb77f48ffd (~100 ETH)
(6) 0x3f7c4c7e727630ad6dc03878d944965e9dbecf0f (~100 ETH)
(7) 0x65f14a0bc2877ed16e82ee7a2324f0c40b1e0b1f (~100 ETH)
(8) 0x60927f6a35057b1d44689fabf64e15efb885c1d9 (~100 ETH)
(9) 0xd0c9f2f34101c8043c9b06147d1686e23b637483 (~100 ETH)

Private Keys
==================
(0) 0x7688087344d4e2c506cd7ccd62f5b54a1eeee7bdfc1f427901510fa5a91f2c01
(1) 0xad5808c13dd6ae7436eeb1268d7fa86c3869489a92245491fa952cb5fd069feb
(2) 0x5dd43826538b49adccf546c80fd11a14db870f1a141e246a1193dfc139af53af
(3) 0x09ff7e4384c4bc9d204d6aabc18e1916c8bec087d5f124213f1442c2368f3642
(4) 0x1da941b3425d2e16ad1da14429ec0a57582575ec1d21b800ffb743b8e128a568
(5) 0xe455445c5cb84a783ff99713649b3a11d3690392b6023f89c86931f29092580c
(6) 0xdc10af9fa5762386b175c67deb158ebccb81a65f4175f13f6774beece57dd82f
(7) 0x083b5956a493aeb90ef7a57e3fd1f81325f5e64b8ab63bdc0598030b85038966
(8) 0x7aec77fa1a631eeb261f7383de8d43bd7d30f28862127162f619960ea66dfd74
(9) 0xe8882b275e082ae8ae80e027dc9a05b801bf2598d011289d604ec914cf4c7733

HD Wallet
==================
Mnemonic:      lizard record awkward sudden open cherry flash answer toilet birth onion zebra
Base HD Path:  m/44'/60'/0'/0/{account_index}

Gas Price
==================
20000000000

Gas Limit
==================
6721975

Listening on 127.0.0.1:8545
net_version
eth_accounts
eth_accounts
eth_accounts
net_version
net_version
eth_sendTransaction

  Transaction: 0x060e29e785269f522433b92683f2fdd984d694e2e6cd91bac96749a92f651e77
  Contract created: 0xa8d9ac1b651606203bcae39eacd41bb309601118
  Gas usage: 277462
  Block Number: 1
  Block Time: Tue Oct 16 2018 01:37:31 GMT+0000 (Coordinated Universal Time)

eth_newBlockFilter
eth_getFilterChanges
eth_getTransactionReceipt
eth_getCode
eth_uninstallFilter
eth_sendTransaction

  Transaction: 0xd0811fcf0b638f4b0bb6cd4c3cd0075bcce2aaa9dde227c3f67a22e5dc5b73d8
  Gas usage: 42008
  Block Number: 2
  Block Time: Tue Oct 16 2018 01:37:31 GMT+0000 (Coordinated Universal Time)

eth_getTransactionReceipt
evm_snapshot
Saved snapshot #1
net_version
eth_sendTransaction

  Transaction: 0xe67c09307436f842ffeafb184bdd2950cb5d18c151688dc9f9dd03cde242ed8e
  Contract created: 0x3a99890b059dd33e3fa3c4be8df280c397a4ffc6
  Gas usage: 1102974
  Block Number: 3
  Block Time: Tue Oct 16 2018 01:37:31 GMT+0000 (Coordinated Universal Time)

eth_newBlockFilter
eth_getFilterChanges
eth_getTransactionReceipt
eth_getCode
eth_uninstallFilter
eth_blockNumber
net_version
eth_call
eth_blockNumber
net_version
eth_call
net_version
eth_call
eth_blockNumber
net_version
eth_sendTransaction

  Transaction: 0xc9443fc4e231b69bb70e3ca77255b772f29dcded60296cec5a87021567668341
  Gas usage: 44056
  Block Number: 4
  Block Time: Tue Oct 16 2018 01:37:32 GMT+0000 (Coordinated Universal Time)

eth_getTransactionReceipt
net_version
eth_sendTransaction

  Transaction: 0x5813ac623f4554b368d840352bcf270bced45f58ba9d0d1de0f7483f33092f64
  Gas usage: 68323
  Block Number: 5
  Block Time: Tue Oct 16 2018 01:37:32 GMT+0000 (Coordinated Universal Time)

eth_getTransactionReceipt
net_version
eth_call
eth_blockNumber
net_version
eth_sendTransaction

  Transaction: 0x5813ac623f4554b368d840352bcf270bced45f58ba9d0d1de0f7483f33092f64
  Gas usage: 22050
  Block Number: 6
  Block Time: Tue Oct 16 2018 01:37:32 GMT+0000 (Coordinated Universal Time)
  Runtime Error: revert

eth_blockNumber
net_version
eth_sendTransaction

  Transaction: 0x5e68a5369e2812430cced68f7fa803d672807eab938558d0a9d8f1e87e56762d
  Gas usage: 44056
  Block Number: 7
  Block Time: Tue Oct 16 2018 01:37:32 GMT+0000 (Coordinated Universal Time)

eth_getTransactionReceipt
net_version
eth_sendTransaction

  Transaction: 0x6c008f9e766195ebe65af6aa95266d2c6afc23d8b9a25215df317633c4c81b5d
  Gas usage: 44056
  Block Number: 8
  Block Time: Tue Oct 16 2018 01:37:32 GMT+0000 (Coordinated Universal Time)

eth_getTransactionReceipt
net_version
eth_sendTransaction

  Transaction: 0x260283d22353cc58d357584c22b3e1073c18eea3b89d71792465c1f88d984b3c
  Gas usage: 55187
  Block Number: 9
  Block Time: Tue Oct 16 2018 01:37:32 GMT+0000 (Coordinated Universal Time)

eth_getTransactionReceipt
net_version
eth_sendTransaction

  Transaction: 0x6d307d20a314fd57419238bc1b7c6fa2c951fe7e1d59b4c0a60caba044c55711
  Gas usage: 22461
  Block Number: 10
  Block Time: Tue Oct 16 2018 01:37:33 GMT+0000 (Coordinated Universal Time)
  Runtime Error: revert

net_version
eth_sendTransaction

  Transaction: 0xfcf89c2de7c64073771c9df073312280d426b786a6d5f13017b39ce4be4d6f62
  Gas usage: 83387
  Block Number: 11
  Block Time: Tue Oct 16 2018 01:37:33 GMT+0000 (Coordinated Universal Time)

eth_getTransactionReceipt
eth_blockNumber
net_version
eth_call
net_version
eth_call
evm_revert
Reverting to snapshot #1
evm_snapshot
Saved snapshot #1
eth_getBalance
eth_getBalance
net_version
eth_sendTransaction

  Transaction: 0x0e86a30800d6feea5890e1057d1274fc277e5bbb22154dfbe0401e09aba92c5e
  Contract created: 0xb8cb885f156035e37f8854a0251d38b7667ea3bc
  Gas usage: 670374
  Block Number: 3
  Block Time: Tue Oct 16 2018 01:37:34 GMT+0000 (Coordinated Universal Time)

eth_newBlockFilter
eth_getFilterChanges
eth_getTransactionReceipt
eth_getCode
eth_getBalance
eth_getBalance
eth_getBalance
eth_uninstallFilter
eth_blockNumber
net_version
eth_call
net_version
eth_sendTransaction

  Transaction: 0x69cc50be20ca3ad7d771418bd583a0aa197874f0125d782aaa44362fe09afbc2
  Gas usage: 48506
  Block Number: 4
  Block Time: Tue Oct 16 2018 01:37:35 GMT+0000 (Coordinated Universal Time)

eth_getBalance
eth_getTransactionByHash
eth_getTransactionReceipt
eth_getBalance
eth_getBalance
eth_getBalance
net_version
eth_sendTransaction

  Transaction: 0x8e301cfc0942a59d5d882a02ae5fe07bd12499c965a9bbea0bc6038c4d133b86
  Gas usage: 44340
  Block Number: 5
  Block Time: Tue Oct 16 2018 01:37:36 GMT+0000 (Coordinated Universal Time)

eth_getTransactionReceipt
eth_getBalance
eth_getBalance
eth_getBalance
```

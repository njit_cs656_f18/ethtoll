'use strict';
const Ballot = artifacts.require("./Ballot.sol");
const truffleAssert = require("truffle-assertions");

contract('Ballot', function(accounts) {

  let BallotInst;
  let chairperson = accounts[0];
  let voter = accounts[1];
  let voter2 = accounts[2];
  let voter_delegates = accounts[3];
  let voter_gets_delegation = accounts[4];


  function bytes2ascii(b32str) {
    return web3.toAscii(b32str).replace(/\0/g, '');
  }
  
  before('getting instance before all Test Cases', async function(){
    BallotInst = await Ballot.new(["P1", "P2", "P3"], {from: chairperson});
  });

  describe('Checking constructor parameters', function(){
    it('should have chairman', async function(){
      assert.equal(await BallotInst.chairperson(), chairperson, 'Test Failed. chairman is not assigned.');
    });
    it('should have three proposals', async function(){
      assert.equal(await BallotInst.getNumberProposals(), 3, 'Test Failed. proposal length incorrect.');
      let proposals = await BallotInst.proposals(0);
      let proposalName = bytes2ascii(proposals[0]);
      assert.equal(proposalName, "P1", "Test failed.  proposal name not correct");
      assert.equal(proposals[1].valueOf(), 0, "Test failed.  proposal vote count not correct");

    });
  });

  describe('Voting', function(){
    it(' should be able to be granted to a voter by the chairperson', async function(){
      await BallotInst.giveRightToVote(voter, {from: chairperson});
      await BallotInst.vote(0, {from: voter});
      let proposals = await BallotInst.proposals(0);
      let proposal_vote_count = proposals[1].valueOf();
      assert.equal(proposal_vote_count, 1, "proposal does not have 1 vote");
    });

    it('a voter can not vote if not permitted', async function(){
      await truffleAssert.fails(BallotInst.vote(0, {from: voter2}),
                          truffleAssert.ErrorType.REVERT,
                          "Voter does not have the right to vote");
    });

    it('permit a valid voter the right to delegate to another', async function(){
      await BallotInst.giveRightToVote(voter_delegates, {from: chairperson});
      await BallotInst.giveRightToVote(voter_gets_delegation, {from: chairperson});
      await BallotInst.delegate(voter_gets_delegation, {from: voter_delegates});
      await truffleAssert.fails(BallotInst.vote(1, {from: voter_delegates}),
                          truffleAssert.ErrorType.REVERT,
                          "Already voted",
                          "Voter who delegates should not have the ability to vote.");
      await BallotInst.vote(1, {from: voter_gets_delegation});
    });

    it('accurately computes the winning proposal', async function(){
      let finalWinningProposalIndex = await BallotInst.winningProposal();
      let finalWinningProposalName = bytes2ascii(await BallotInst.winnerName());
      assert.equal(finalWinningProposalIndex, 1, "P2 did not win");
      assert.equal(finalWinningProposalName, "P2", "P2 did not win");
    });
  });

});

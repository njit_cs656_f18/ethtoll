'use strict';
const Purchase = artifacts.require("./Purchase.sol");

contract('Purchase', function(accounts) {

  let PurchaseInst;
  let seller = accounts[5];
  let buyer = accounts[6];

  function bytes2ascii(b32str) {
    return web3.toAscii(b32str).replace(/\0/g, '');
  }

  async function conductTx(meth, buyer, amount) {
    const hash = await meth.sendTransaction({from: buyer, value: amount});
    return getTxDetail(buyer, hash);
  }

  async function getTxDetail(buyer, hash) {
    const balanceAfter = web3.eth.getBalance(buyer);
    const tx = await web3.eth.getTransaction(hash);
    const receipt = await web3.eth.getTransactionReceipt(hash);
    const gasCost = await tx.gasPrice.mul(await receipt.gasUsed);
    return [balanceAfter, gasCost, hash, tx, receipt];
  }

  function logBalance(evt_label, label, addr) {
    let b = web3.fromWei(web3.eth.getBalance(addr)).toString();
    console.log(evt_label, "the", label, "has", b, "ether");
  }
  
  before('A seller wants to sell an item for 1 ether', async function() {
    logBalance("before any transaction", "seller", seller);
    logBalance("before any transaction", "buyer", buyer);
    PurchaseInst = await Purchase.new({from: seller, value: web3.toWei(2, "ether")});
    logBalance("after posting the sale", "seller", seller);
    logBalance("after posting the sale", "buyer", buyer);
    logBalance("after posting the sale", "contract", PurchaseInst.address);
  });

  describe('A buyer buys an item for 1 ether with a 1 ether deposit.', function(){
    it(' confirming the purchase whose item value is 1 ether, with a 1 ether deposit', async function(){
      let purchaseValue = await PurchaseInst.value.call();
      assert.equal(purchaseValue, web3.toWei(1, "ether"), "Value is half the list price, but seller sends full amount");
      let [b, gc, t, hash, r] = await conductTx(PurchaseInst.confirmPurchase, buyer, web3.toWei(2, "ether"));
      console.log("and after confirming the transaction the buyer has", web3.fromWei(b, 'ether').toString(), "ether");
      console.log("using ", web3.fromWei(gc, 'ether').toString(), " gas in ether conducting the transaction.");
      logBalance("after purchase confirmation", "buyer", buyer);
      logBalance("after purchase confirmation", "seller", seller);
      logBalance("after purchase the sale", "contract", PurchaseInst.address);
      await PurchaseInst.confirmReceived({from: buyer});
      logBalance("after item receipt confirmation", "buyer", buyer);
      logBalance("after item receipt confirmation", "seller", seller);
      logBalance("after item receipt confirmation", "contract", PurchaseInst.address);
    });
  });
});


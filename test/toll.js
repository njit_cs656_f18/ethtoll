'use strict';
const Toll = artifacts.require("./Toll.sol");
const truffleAssert = require("truffle-assertions");

contract('Toll', function(accounts) {

  let TollInst;
  let chairperson = accounts[0];
  let voter = accounts[1];
  let voter2 = accounts[2];
  let maintainer1 = accounts[3];
  let maintainer2 = accounts[4];
  let car1 = accounts[5];
  let car2 = accounts[6];


  function bytes2ascii(b32str) {
    return web3.toAscii(b32str).replace(/\0/g, '');
  }

  async function conductTx(meth, buyer, amount) {
    const hash = await meth.sendTransaction({from: buyer, value: amount});
    return getTxDetail(buyer, hash);
  }

  async function getTxDetail(buyer, hash) {
    const balanceAfter = web3.eth.getBalance(buyer);
    const tx = await web3.eth.getTransaction(hash);
    const receipt = await web3.eth.getTransactionReceipt(hash);
    const gasCost = await tx.gasPrice.mul(await receipt.gasUsed);
    return [balanceAfter, gasCost, hash, tx, receipt];
  }

  function logBalance(evt_label, label, addr) {
    let b = web3.fromWei(web3.eth.getBalance(addr)).toString();
    console.log(evt_label, "the", label, "has", b, "ether");
  }
  
  before('getting instance before all Test Cases', async function(){
    TollInst = await Toll.new(["P1", "P2"], 
                                [maintainer1, maintainer2], 
                                [web3.toWei(2, "ether"),
                                 web3.toWei(1, "ether")], {from: chairperson});
  });


  describe('Toll Contract Lifecycle', function(){
    it('voting granted by chairperson, but chairperson can be outvoted', async function(){
      // The chairman can give the right to vote to voters.
      await TollInst.giveRightToVote(voter, {from: chairperson});
      await TollInst.giveRightToVote(voter2, {from: chairperson});

      // Cars cannot pay tolls (to this contract) before the vote is finalized.
      await truffleAssert.fails(
          TollInst.payToll.sendTransaction({from: car1, value: 4}),
          truffleAssert.ErrorType.REVERT,
          "A proposal has not yet been awarded");
      // Maintainers cannot submit invoices.
      await truffleAssert.fails(
          TollInst.submitInvoice(10, "InvoiceR1", {from: maintainer1}),
          truffleAssert.ErrorType.REVERT,
          "A proposal has not yet been awarded");
      await truffleAssert.fails(TollInst.vote(0, {from: voter2}),
                          truffleAssert.ErrorType.REVERT,
                          "Voting has not yet started");
      await truffleAssert.fails(TollInst.startVoting({from: voter2}),
                          truffleAssert.ErrorType.REVERT,
                          "Only chairperson can start the vote");
      await TollInst.startVoting({from: chairperson});
      await TollInst.vote(0, {from: chairperson});
      await TollInst.vote(1, {from: voter});
      await truffleAssert.fails(TollInst.endVote({from: chairperson}),
                          truffleAssert.ErrorType.REVERT,
                          "Not all voters have voted.");
      await TollInst.vote(1, {from: voter2});
      await TollInst.endVote({from: chairperson});
      let finalWinningProposalIndex = await TollInst.winningProposal();
      let proposals = await TollInst.proposals(finalWinningProposalIndex);
      let proposal_vote_count = proposals[1].valueOf();
      assert.equal(proposal_vote_count, 2, "proposal does not have 1 vote");
    });

    it('after contract award only permit the awardee the ability to submit invoices', async function(){
      // We do not permit someone not awarded a contract to submit invoices      
      await truffleAssert.fails(
          TollInst.submitInvoice(1, "InvoiceR1", {from: maintainer1}),
          truffleAssert.ErrorType.REVERT,
          "Only the awarded maintainer can submit invoices");
    });

    it('a car must pay an amount equal to the toll', async function(){
      
      // A car must pay an amount equal to the toll, not less
      await truffleAssert.fails(
          TollInst.payToll.sendTransaction({from: car1, value:1}),
          truffleAssert.ErrorType.REVERT,
          "Not equal to toll amount");
      
      // A car must pay an amount equal to the toll, not more
      await truffleAssert.fails(
          TollInst.payToll.sendTransaction({from: car1, value:web3.toWei("4", "ether")}),
          truffleAssert.ErrorType.REVERT,
          "Not equal to toll amount");

    });

    it('allows cars to pay tolls after contract award', async function(){
      let beforeLabel = "before paying the toll 4 times";
      logBalance(beforeLabel, "a car", car2);
      logBalance(beforeLabel, "contract", TollInst.address);
      logBalance(beforeLabel, "maintainer", maintainer2);
 
      for(var i = 0; i < 4; i++) {
        await TollInst.payToll.sendTransaction({from: car2, value: web3.toWei("1", "ether")});
      }
      
      let afterLabel = "after paying the toll 4 times";
      logBalance(afterLabel, "a car", car2);
      logBalance(afterLabel, "contract", TollInst.address);
      logBalance(afterLabel, "maintainer", maintainer2);

      assert.equal(web3.eth.getBalance(TollInst.address), web3.toWei("4", "ether"), "does not have correct amount of ether on contract.");

    });

    it('does not allow the maintainer submit invoices for more than the contract', async function(){
      await truffleAssert.fails(
          TollInst.submitInvoice(web3.toWei("10", "ether"), "InvoiceF1", {from: maintainer2}),
          truffleAssert.ErrorType.REVERT,
          "Can't bleed blood from a rock! Wait for more!");


    });

    it('allows the maintainer contract awardee to submit invoices', async function(){
      let beforeLabel = "before submitting invoice";
      logBalance(beforeLabel, "a car", car2);
      logBalance(beforeLabel, "contract", TollInst.address);
      logBalance(beforeLabel, "maintainer", maintainer2);

      await TollInst.submitInvoice(web3.toWei("1", "ether"), "InvoiceR1", {from: maintainer2});

      let afterLabel = "after submitting invoice";
      logBalance(afterLabel, "a car", car2);
      logBalance(afterLabel, "contract", TollInst.address);
      logBalance(afterLabel, "maintainer", maintainer2);

    });

    it('disallows anyone but the chairperson from approving invoices', async function(){

      await truffleAssert.fails(
          TollInst.approveInvoice({from: maintainer2}),
          truffleAssert.ErrorType.REVERT,
          "Only the chairperson can approve invoices");
    });

    it('allows the chairperson to approve an invoice', async function(){
      let beforeLabel = "before approving invoice";
      logBalance(beforeLabel, "a car", car2);
      logBalance(beforeLabel, "chairperson", chairperson);
      logBalance(beforeLabel, "contract", TollInst.address);
      logBalance(beforeLabel, "maintainer", maintainer2);

      await TollInst.approveInvoice({from: chairperson});

      let afterLabel = "after approving invoice";
      logBalance(afterLabel, "a car", car2);
      logBalance(afterLabel, "chairperson", chairperson);
      logBalance(afterLabel, "contract", TollInst.address);
      logBalance(afterLabel, "maintainer", maintainer2);

    });
  });

});
